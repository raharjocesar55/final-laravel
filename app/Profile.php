<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profile";
    protected $fillable = ['nama', 'bio', 'emailProfile', 'umur', 'alamat', 'gambar', 'users_id'];
}
