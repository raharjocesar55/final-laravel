<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = "pertanyaan"; 
    protected $fillable = ['konten', 'gambar', 'kategori_id', 'users_id'];
}