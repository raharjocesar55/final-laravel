<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Kategori;
use File;

class KategoriController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    public function create()
    {
        $kategori = Kategori::all();
        return view('kategori/create', compact('kategori'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama'  => 'required'
        ],
        [
            'nama.required'     => 'Nama Kategori Tidak Boleh Kosong !'
        ]);

        $kategori = new Kategori;
        $kategori->nama = $request->nama;
        $kategori->save();

        return redirect('/kategori');
    }

    public function index()
    {
        $kategori = kategori::all();
        return view('/kategori/index', compact('kategori'));
    }

    // public function show($id)
    // {
    //     $kategori = Kategori::findorfail($id);
    //     return view('/kategori/show', compact('kategori'));
    // }

    public function edit($id)
    {
        $kategori = Kategori::all();
        $kategori = Kategori::findorfail($id);
        return view('/kategori/edit', compact('kategori'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama'  => 'required',
        ],
        [
            'nama.required'     => 'Nama Kategori Tidak Boleh Kosong !',
        ]);

        $kategori = Kategori::findorfail($id);
        
            $kategori_data = [
                'nama' => $request->nama,
            ];

        $kategori->update($kategori_data);

        return redirect('/kategori');
    }

    public function destroy($id)
    {
        $kategori = Kategori::findorfail($id);
        $kategori->delete();
        
        return redirect('/kategori');
    }
}
