<?php

namespace App\Http\Controllers;

use App\Pertanyaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LandingController extends Controller
{
    public function index()
    {
        // $pertanyaan = Pertanyaan::all();
        $pertanyaan = DB::table('pertanyaan')->join('users', 'pertanyaan.users_id', '=', 'users.id')->select('pertanyaan.*', 'users.name as username')->get();
        // dd($pertanyaan);
        return view('landing/index', compact('pertanyaan'));
    }

    public function show()
    {
        return view('/landing/show');
    }
}
