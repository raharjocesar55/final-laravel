<?php

namespace App\Http\Controllers;

use App\jawaban;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Pertanyaan;
use App\Kategori;
use File;

class PertanyaanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function create()
    {
        $kategori = Kategori::all();
        return view('pertanyaan/create', compact('kategori'));
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'konten'  => 'required',
                'gambar'   => 'required|mimes:png,jpg,jpeg|max:2048',
            ],
            [
                'konten.required'     => 'Konten Tidak Boleh Kosong !',
                'gambar.required'      => 'Gambar Tidak Boleh Kosong !',
            ]
        );

        $fileName = time() . '.' . $request->gambar->extension();
        $pertanyaan = new Pertanyaan;
        $pertanyaan->konten = $request->konten;
        $pertanyaan->kategori_id = $request->kategori_id;
        $pertanyaan->users_id = $request->user_id;
        $pertanyaan->gambar = $fileName;
        $pertanyaan->save();
        $request->gambar->move(public_path('image'), $fileName);

        return redirect('/pertanyaan');
    }

    public function index()
    {
        $pertanyaan = Pertanyaan::all();
        return view('/pertanyaan/index', compact('pertanyaan'));
    }

    public function show($id)
    {
        $pertanyaan = Pertanyaan::findorfail($id);
        // query join tabel users, profile, pertanyaan, jawaban
        $komen = DB::table('jawaban')->join('users', 'jawaban.users_id', '=', 'users.id')->join('pertanyaan', 'jawaban.pertanyaan_id', '=', 'pertanyaan.id')->join('profile', 'users.id', '=', 'profile.users_id')->select('jawaban.jawaban', 'users.id as uid', 'profile.gambar as foto', 'profile.nama as nama')->where('pertanyaan_id', $id)->get();
        // dd($komen);
        return view('/pertanyaan/show', compact('pertanyaan', 'komen'));
    }

    public function edit($id)
    {
        $kategori = Kategori::all();
        $pertanyaan = Pertanyaan::all();
        $pertanyaan = Pertanyaan::findorfail($id);
        return view('/pertanyaan/edit', compact('pertanyaan', 'kategori'));
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'konten'  => 'required',
                'gambar'   => 'mimes:png,jpg,jpeg|max:2048',
            ],
            [
                'konten.required'     => 'Konten Tidak Boleh Kosong !',
                'gambar.required'      => 'Gambar Tidak Boleh Kosong !',
            ]
        );

        $pertanyaan = Pertanyaan::findorfail($id);

        if ($request->has('gambar')) {
            $path = "image/";
            File::delete($path . $pertanyaan->gambar);
            $fileName = time() . '.' . $request->gambar->extension();
            $request->gambar->move(public_path('image'), $fileName);

            $pertanyaan_data = [
                'konten' => $request->konten,
                'gambar' => $fileName,
            ];
        } else {
            $pertanyaan_data = [
                'konten' => $request->konten,
            ];
        }

        $pertanyaan->update($pertanyaan_data);

        return redirect('/pertanyaan');
    }

    public function destroy($id)
    {
        $pertanyaan = Pertanyaan::findorfail($id);
        $path = "image/";
        File::delete($path . $pertanyaan->gambar);
        Jawaban::where('pertanyaan_id', $id)->delete();
        $pertanyaan->delete();

        return redirect('/pertanyaan');
    }
}
