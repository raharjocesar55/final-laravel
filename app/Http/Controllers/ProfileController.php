<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Profile;
use File;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function create()
    {
        $profile = Profile::all();
        return view('profile/create', compact('profile'));
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'nama'  => 'required',
                'bio'  => 'required',
                'emailProfile'   => 'required',
                'umur'   => 'required',
                'alamat'   => 'required',
                'gambar'   => 'required|mimes:png,jpg,jpeg|max:2048',
            ],
            [
                'nama.required'     => 'Nama Tidak Boleh Kosong !',
                'bio.required'     => 'Biodata Tidak Boleh Kosong !',
                'emailProfile.required'      => 'Email Tidak Boleh Kosong !',
                'umur.required'      => 'Umur Tidak Boleh Kosong !',
                'alamat.required'      => 'Alamat Tidak Boleh Kosong !',
                'gambar.required'      => 'Gambar Tidak Boleh Kosong !',
            ]
        );

        $fileName = time() . '.' . $request->gambar->extension();
        $profile = new Profile;
        $profile->nama = $request->nama;
        $profile->bio = $request->bio;
        $profile->emailProfile = $request->emailProfile;
        $profile->umur = $request->umur;
        $profile->alamat = $request->alamat;
        $profile->gambar = $fileName;
        $profile->save();
        $request->gambar->move(public_path('image'), $fileName);

        return redirect('/profile');
    }

    public function index()
    {
        $profile = Profile::first();
        // dd($profile->nama);
        return view('/profile/index', compact('profile'));
    }

    public function show($id)
    {
        $profile = Profile::findorfail($id);
        return view('/profile/show', compact('profile'));
    }

    public function edit($id)
    {
        $profile = Profile::all();
        $profile = Profile::findorfail($id);
        return view('/profile/edit', compact('profile'));
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama'  => 'required',
                'bio'  => 'required',
                'emailProfile'   => 'required',
                'umur'   => 'required',
                'alamat'   => 'required',
                'gambar'   => 'mimes:png,jpg,jpeg|max:2048',
            ],
            [
                'nama.required'     => 'Nama Tidak Boleh Kosong !',
                'bio.required'     => 'Biodata Tidak Boleh Kosong !',
                'emailProfile.required'      => 'Email Tidak Boleh Kosong !',
                'umur.required'      => 'Umur Tidak Boleh Kosong !',
                'alamat.required'      => 'Alamat Tidak Boleh Kosong !',
                'gambar.required'      => 'Gambar Tidak Boleh Kosong !',
            ]
        );

        $profile = Profile::findorfail($id);
        // dd($request->id);

        if ($request->has('gambar')) {
            $path = "image/";
            File::delete($path . $profile->gambar);
            $fileName = time() . '.' . $request->gambar->extension();
            $request->gambar->move(public_path('image'), $fileName);

            $profile_data = [
                // 'id' => $request->id,
                'nama' => $request->nama,
                'bio' => $request->bio,
                'emailProfile' => $request->emailProfile,
                'umur' => $request->umur,
                'alamat' => $request->alamat,
                'gambar' => $fileName,
            ];
        } else {
            $profile_data = [
                // 'id' => $request->id,
                'nama' => $request->nama,
                'bio' => $request->bio,
                'emailProfile' => $request->emailProfile,
                'umur' => $request->umur,
                'alamat' => $request->alamat,
            ];
        }

        // $pro = new Profile;
        $profile->update($profile_data);

        return redirect('/profile');
    }

    public function destroy($id)
    {
        $profile = Profile::findorfail($id);
        $path = "image/";
        File::delete($path . $profile->gambar);
        $profile->delete();

        return redirect('/profile');
    }
}
