<?php

namespace App\Http\Controllers;

use App\jawaban;
use Illuminate\Http\Request;


class JawabanController extends Controller
{
    public function store(Request $data)
    {
        # code...
        $jawaban = new jawaban;
        $jawaban->jawaban = $data->komen;
        $jawaban->pertanyaan_id = $data->pertanyaan_id;
        $jawaban->users_id = $data->user_id;
        $jawaban->save();

        return redirect('/pertanyaan' . '/' . $data->pertanyaan_id);
        // dd($data);
    }
}
