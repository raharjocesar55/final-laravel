<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index');

    // profile
    Route::get('/profile', 'ProfileController@index');
    Route::get('/profile/create', 'ProfileController@create');
    Route::post('/profile', 'ProfileController@store');
    Route::get('/profile/{profile_id}', 'ProfileController@show');
    Route::get('/profile/{profile_id}/edit', 'ProfileController@edit');
    Route::put('/profile/{profile_id}', 'ProfileController@update');
    Route::delete('/profile/{profile_id}', 'ProfileController@destroy');

    // kategori
    Route::get('/kategori', 'KategoriController@index');
    Route::get('/kategori/create', 'KategoriController@create');
    Route::post('/kategori', 'KategoriController@store');
    Route::get('/kategori/{kategori_id}', 'KategoriController@show');
    Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
    Route::put('/kategori/{kategori_id}', 'KategoriController@update');
    Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');

    // pertanyaan
    Route::get('/pertanyaan', 'PertanyaanController@index');
    Route::get('/pertanyaan/create', 'PertanyaanController@create');
    Route::post('/pertanyaan', 'PertanyaanController@store');
    Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
    Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit');
    Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');
    Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');

    // jawaban
    Route::post('/jawaban', 'JawabanController@store');
});

Route::get('/landing', 'LandingController@index');
Route::get('/landing/show', 'LandingController@show');

Auth::routes();
