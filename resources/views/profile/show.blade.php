@extends('/partials/master')

@section('title')
    Data Profile Detail
@endsection

@section('judul')
    Profile Detail
@endsection

@section('content')
    <div class="card" style="width: 18rem;">
        <img src="{{asset('image/'.$profile->gambar)}}" class="card-img-top">
        <div class="card-body">
            <h1>{{$profile->nama}}</h1>
            <p class="card-text">{{$profile->bio}}</p>
            <p class="card-text">Email: {{$profile->emailProfile}}</p>
            <p class="card-text">Umur: {{$profile->umur}}</p>
            <p class="card-text">Alamat: {{$profile->alamat}}</p>
            <a href="/profile" class="btn btn-info">Kembali</a>
        </div>
    </div>
@endsection