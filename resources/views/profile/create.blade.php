@extends('/partials/master')

@section('title')
    Data Profile Baru
@endsection

@section('judul')
    Profile Baru
@endsection

@section('content')
    <form action="/profile" method="POST" enctype="multipart/form-data">
        @csrf
        <h1>Tambah Profile Baru</h1>

        <div class="mb-3">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="mb-3">
            <label>Biodata</label>
            <textarea name="bio" class="form-control" rows="3"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="mb-3">
            <label>Email</label>
            <input type="text" name="emailProfile" class="form-control">
        </div>
        @error('emailProfile')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="mb-3">
            <label>Umur</label>
            <input type="text" name="umur" class="form-control">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="mb-3">
            <label>Alamat</label>
            <textarea name="alamat" class="form-control" rows="3"></textarea>
        </div>
        @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="mb-3">
            <label>Photo</label>
            <input type="file" class="form-control" name="gambar">
        </div>
        @error('gambar')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="col-12">
            <button type="submit" class="btn btn-primary">Tambah Data</button>
            <a href="/profile" class="btn btn-danger">Batal</a>
        </div>
    </form>
@endsection