@extends('/partials/master')

@section('title')
List Data Profile
@endsection

@section('judul')
Profile Pengguna
@endsection

@section('content')
<div class="container-xl px-4 mt-4">
    <!-- Account page navigation-->
    <div class="row">
        <div class="col-xl-4">
            <!-- Profile picture card-->
            <div class="card mb-4 mb-xl-0">
                <div class="card-header">Profile Picture</div>
                <div class="card-body text-center">
                    <!-- Profile picture image-->
                    <img class="card-img-top" src="{{asset('image/'.$profile->gambar)}}">
                    <!-- Profile picture help block-->
                    <!-- <div class="small font-italic text-muted mb-4">JPG or PNG no larger than 5 MB</div> -->
                    <!-- Profile picture upload button-->
                    <!-- <button class="btn btn-primary" type="button">Upload new image</button> -->
                </div>
            </div>
        </div>
        <div class="col-xl-8">
            <!-- Account details card-->
            <div class="card mb-4">
                <div class="card-header">Profile Details</div>
                <div class="card-body">
                    <form action="/profile/{{$profile->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <!-- Form Group (nama)-->
                        <div class="mb-3">
                            <label class="small mb-1" for="nama">Nama</label>
                            <input name="nama" class="form-control" id="nama" type="text" placeholder="Enter your username" value="{{ $profile->nama }}">
                        </div>
                        @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <!-- Form Group (alamat)-->
                        <div class="mb-3">
                            <label class="small mb-1" for="alamat">Alamat</label>
                            <input name="alamat" class="form-control" id="alamat" type="text" placeholder="Enter your username" value="{{ $profile->alamat }}">
                        </div>
                        @error('alamat')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <!-- Form Group (umur)-->
                        <div class="mb-3">
                            <label class="small mb-1" for="umur">Umur</label>
                            <input name="umur" class="form-control" id="umur" type="number" placeholder="Enter your username" value="{{ $profile->umur }}">
                        </div>
                        @error('umur')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <!-- Form Group (email address)-->
                        <div class="mb-3">
                            <label class="small mb-1" for="emailProfile">Email address</label>
                            <input name="emailProfile" class="form-control" id="emailProfile" type="email" placeholder="Enter your email address" value="{{ $profile->emailProfile }}">
                        </div>
                        @error('emailProfile')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <!-- Form Group (biodata)-->
                        <div class="mb-3">
                            <label class="small mb-1" for="bio">Biodata</label>
                            <textarea name="bio" id="bio" class="form-control" style="resize: none;" rows="20">{{ $profile->bio }}</textarea>
                        </div>
                        @error('bio')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="mb-3">
                            <label>Photo</label>
                            <input type="file" id="sampul" class="form-control" name="gambar" onchange="previewImg();">
                        </div>
                        <!-- Save changes button-->
                        <button class="btn btn-primary" type="submit">Perbaharui Profile</button>
                        <a href="/profile" class="btn btn-danger">Batal</a>
                        <!-- <a class="btn btn-primary" href="/profile/{{ $profile->id }}/edit">Edit</a> -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    // alert("wow");

    function previewImg() {
        const sampul = document.querySelector('#sampul');
        const imgPreview = document.querySelector('.img-preview-ku');

        const fileSampul = new FileReader();
        fileSampul.readAsDataURL(sampul.files[0]);

        fileSampul.onload = function(e) {
            imgPreview.src = e.target.result;
        }
    }
</script>
@endpush