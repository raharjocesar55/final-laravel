@extends('/partials/master')

@section('title')
List Pertanyaan
@endsection

@section('judul')
Pertanyaan Forum Diskusi
@endsection


@section('content')

<!--<section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <h2>Search the question</h2>
        <ul class="nav">
            <li><input type="text" name="search" class="form-control"></li>
            <li><button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button></li>
        </ul>
      </div>
    </section>-->

<section class="inner-page">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                @foreach($pertanyaan as $key => $p)
                <div class="card mb-2">
                    <div class="card-body">
                        <h3><a href="">Question#{{ $key+1 }}</a></h3>
                        <p class="small"><b>{{ $p->username }}</b></p>
                        <p>{{ $p->konten }}</p>

                        <a href="/landing/show" class="small">see responses(3) |</a>
                        <a href="#" class="small">reply |</a>
                    </div>
                </div>
                @endforeach
                <!-- <div class="card">
                    <div class="card-body">
                        <h3><a href="">Question#239</a></h3>
                        <p class="small"><b>user+62</b></p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid, unde?</p>

                        <a href="/landing/show" class="small">see responses(12) |</a>
                        <a href="#" class="small">reply |</a>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</section>
@endsection