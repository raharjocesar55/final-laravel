@extends('/partials/master')

@section('title')
    List Pertanyaan
@endsection

@section('judul')
    Pertanyaan Forum Diskusi
@endsection


@section('content')

    <!--<section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <h2>Search the question</h2>
        <ul class="nav">
            <li><input type="text" name="search" class="form-control"></li>
            <li><button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button></li>
        </ul>
      </div>
    </section>-->

    <section class="inner-page">
      <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card mb-2">     
                    <div class="card-body">
                    <h3><a href="">Question#238</a></h3>
                    <p class="small"><b>user+62</b></p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam non qui ullam natus quaerat velit earum, iste nemo hic tempora error laborum aliquam, optio, ratione dolorum minus architecto sunt labore aut exercitationem fugit voluptatibus! Laudantium iste eum, pariatur quidem ad iure velit aliquid inventore. Natus iste sint doloremque expedita possimus...</p>
                        <div class="row">
                            <div class="col-10">
                                <input type="text" class="form-control">
                            </div>
                            <div class="col-2">
                                <button class="btn btn-primary">Jawab</button>
                            </div>
                        </div>
                    <div class="card mb-2 mt-2">     
                        <div class="card-body">
                        <h3><a href="">Answer#1</a></h3>
                        <p class="small"><b>user+22</b></p>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Qui nostrum ea ad repellendus magnam vero placeat adipisci itaque quidem dolor.</p>
                        </div>
                    </div>
                    <div class="card mb-2 mt-2">     
                        <div class="card-body">
                        <h3><a href="">Answer#2</a></h3>
                        <p class="small"><b>user+22</b></p>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Qui nostrum ea ad repellendus magnam vero placeat adipisci itaque quidem dolor.</p>
                        </div>
                    </div>
                    <div class="card mb-2 mt-2">     
                        <div class="card-body">
                        <h3><a href="">Answer#3</a></h3>
                        <p class="small"><b>user+56</b></p>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Qui nostrum ea ad repellendus magnam vero placeat adipisci itaque quidem dolor.</p>
                        </div>
                    </div>   
                </div>
                </div>
            </div>
        </div>
      </div>
    </section>
@endsection