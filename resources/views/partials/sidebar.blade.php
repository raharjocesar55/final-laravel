<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="#" class="brand-link">
    <img src="{{asset('/assets/icon.png')}}" alt="Logo SanberCode" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">Forum Tanya-Jawab</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="info">
        @auth
        <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        @endauth
        @guest
        <a href="#" class="d-block">Belum Login</a>
        @endguest

      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="/" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>Dashboard</p>
          </a>
        </li>
        @auth
        <li class="nav-item">
          <a href="/profile" class="nav-link">
            <i class="nav-icon fas fa-user-alt"></i>
            <p>Profile</p>
          </a>
        </li>
        @endauth

        @auth
        <li class="nav-item">
          <a href="/kategori" class="nav-link">
            <i class="nav-icon fas fa-newspaper"></i>
            <p>Kategori</p>
          </a>
        </li>
        @endauth

        @auth
        <li class="nav-item">
          <a href="/pertanyaan" class="nav-link">
            <i class="nav-icon fab fa-dropbox"></i>
            <p>Ajukan Pertanyaan</p>
          </a>
        </li>
        @endauth

        @auth
        <li class="nav-item">
          <a href="/landing" class="nav-link">
            <i class="nav-icon fas fa-users"></i>
            <p>Diskusi Forum</p>
          </a>
        </li>
        @endauth

        @auth
        <li class="nav-item bg-danger">
          <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
            Logout
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </li>
        @endauth

        @guest
        <li class="nav-item bg-primary">
          <a href="/login" class="nav-link">
            <p>Login</p>
          </a>
        </li>
        @endguest

      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>