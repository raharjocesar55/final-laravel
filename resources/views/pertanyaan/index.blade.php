@extends('/partials/master')

@section('title')
List Pertanyaan
@endsection

@section('judul')
Pertanyaan Forum Diskusi
@endsection

@section('content')

<a href="/pertanyaan/create" class="btn btn-primary mb-3"><i class="fas fa-plus"></i> Tambah Pertanyaan</a>

@forelse ($pertanyaan as $item)
<div class="card" style="width: 18rem;">
    <img src="{{asset('image/'.$item->gambar)}}" class="card-img-top">
    <div class="card-body">
        <h5 class="card-title">Pertanyaan#{{$item->id}}</h5>
        <p class="card-text">{{Str::limit($item -> konten, 50)}}</p>
        <form action="/pertanyaan/{{$item->id}}" method="POST">
            @csrf
            @method('delete')
            <a href="/pertanyaan/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/pertanyaan/{{$item->id}}/edit" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i> Edit</a>
            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
        </form>
    </div>
</div>
@empty
<h5>Data Masih Kosong !</h5>
@endforelse

@endsection