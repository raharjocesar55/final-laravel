@extends('/partials/master')

@section('title')
    Detail Pertanyaan
@endsection

@section('judul')
    Isi Pertanyaan Anda
@endsection

@section('content')
<div class="card">
    <img src="{{asset('image/'.$pertanyaan->gambar)}}" class="card-img-top">
    <div class="card-body">
        <!-- <h1>Nama Pertanyaan</h1> -->
        <p class="card-text mb-5">{{ $pertanyaan->konten }}</p>
        @foreach($komen as $k)
        @if($k->uid == Auth::user()->id)
        <div class="card mb-3 text-right border border-primary shadow-none">
            <div class="card-header">
                <a href="/profile/{{ $k->uid }}">{{ $k->nama }}
                    <img src="{{ asset('/image/'.$k->foto) }}" class="rounded-circle ml-2" alt="..." style="width: 35px;">
                </a>
            </div>
            <div class="card-body">
                <p class="card-text">{{ $k->jawaban }}</p>
            </div>
        </div>
        @else
        <div class="card mb-3 text-left border border-warning shadow-none">
            <div class="card-header">
                <a href="/profile/{{ $k->uid }}">
                    <img src="{{ asset('/image/'.$k->foto) }}" class="rounded-circle mr-2" alt="..." style="width: 35px;">
                    {{ $k->nama }}
                </a>
            </div>
            <div class="card-body">
                <p class="card-text">{{ $k->jawaban }}</p>
            </div>
        </div>
        @endif
        @endforeach

        <div class="mb-3">
            <form method="post" action="/jawaban" class="row g-3">
                @csrf
                <div class="col-11">
                    <input type="text" name="komen" class="form-control" id="komen" placeholder="Tiggalkan Komentar Anda disini..." oninput="holdKirim();">
                </div>
                <input type="hidden" name="pertanyaan_id" value="{{ $pertanyaan->id }}">
                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                <div class="col-1">
                    <button type="submit" name="kirim" id="kirim" class="btn btn-primary mb-3" disabled>Kirim</button>
                </div>
            </form>
        </div>
        <a href="/pertanyaan" class="btn btn-info">Kembali</a>
    </div>
</div>
@endsection

@push('scripts')
<script>
    function holdKirim() {
        const tmKirim = document.querySelector('#kirim');
        const txtKomen = document.querySelector('#komen');
        if (txtKomen.value == '') {
            tmKirim.disabled = true;
        } else {
            tmKirim.disabled = false;
        }
    }
</script>
@endpush