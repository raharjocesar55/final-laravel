@extends('/partials/master')

@section('title')
Data Pertanyaan Baru
@endsection

@section('judul')
Pertanyaan Baru
@endsection

@section('content')
<form action="/pertanyaan" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="mb-3">
        <label>Buat Pertanyaan</label>
        <textarea name="konten" class="form-control" placeholder="Enter your new question" rows="3"></textarea>
    </div>
    @error('konten')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="mb-3">
        <img src="{{ asset('image/default.jpg') }}" alt="default" class="img-preview-ku">
    </div>

    <div class="mb-3">
        <label>Gambar</label>
        <input type="file" class="form-control" name="gambar" id="sampul" onchange="previewImg();">
    </div>
    @error('gambar')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="mb-3">
        <label>Kategori</label>
        <select name="kategori_id" class="form-control" id="">
            <option value="">--- Pilih Kategori Pertanyaan ---</option>
            @foreach ($kategori as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
    </div>
    @error('kategori_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
    <div class="col-12">
        <button type="submit" class="btn btn-primary">Tambah Pertanyaan</button>
        <a href="/pertanyaan" class="btn btn-danger">Batal</a>
    </div>
</form>
@endsection

@push('scripts')
<script>
    // alert("wow");

    function previewImg() {
        const sampul = document.querySelector('#sampul');
        const imgPreview = document.querySelector('.img-preview-ku');

        const fileSampul = new FileReader();
        fileSampul.readAsDataURL(sampul.files[0]);

        fileSampul.onload = function(e) {
            imgPreview.src = e.target.result;
        }
    }
</script>
@endpush