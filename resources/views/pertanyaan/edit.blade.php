@extends('/partials/master')

@section('title')
    Data Pertanyaan Edit
@endsection

@section('judul')
    Pertanyaan Edit
@endsection

@push('scripts')
    <script>
        Swal.fire({
            title: "Berhasil!",
            text: "Data berhasil diperbaharui!",
            icon: "success",
            confirmButtonText: "Lanjutkan",
        });
    </script>
@endpush

@section('content')
    <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')

        <div class="mb-3">
            <label>Perbaharui Pertanyaan</label>
            <textarea name="konten" class="form-control" rows="3">{{$pertanyaan->konten}}</textarea>
        </div>
        @error('konten')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="mb-3">
            <label>Gambar</label>
            <input type="file" class="form-control" name="gambar">
        </div>

        <div class="mb-3">
            <label>Kategori</label>
            <select name="kategori_id" class="form-control" id="">
                <option value="">--- Pilih Kategori Pertanyaan ---</option>
                @foreach ($kategori as $item)
                    @if ($item->id === $pertanyaan->kategori_id)
                        <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                    @else
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        @error('kategori_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="col-12">
            <button type="submit" class="btn btn-primary">Perbaharui Pertanyaan</button>
            <a href="/pertanyaan" class="btn btn-danger">Batal</a>
        </div>
    </form>
@endsection