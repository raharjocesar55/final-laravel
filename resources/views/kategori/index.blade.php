@extends('/partials/master')

@section('title')
    List Data Kategori
@endsection

@section('judul')
    Kategori
@endsection


@section('content')

<!--<a href="/profile/create" class="btn btn-primary mb-3">Tambah Profile Baru</a>-->
<a href="/kategori/create" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah Kategori</a>
<table id="dttbl" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Nama Kategori</th>
            <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
    @forelse ($kategori as $key => $item)
            <tr>
                <td>{{$key + 1}}</td> 
                <td>{{$item -> nama}}</td> 
                <td>
                    <form action="/kategori/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <!-- <a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm">Detail</a> -->
                        <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i> Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>

       @empty
            <tr>
                <td>Data Masih Kosong !</td>
            </tr>
       @endforelse
    </tbody>
</table>

@endsection