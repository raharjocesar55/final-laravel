@extends('/partials/master')

@section('title')
    Data Kategori Edit
@endsection

@section('judul')
    Kategori Edit
@endsection

@push('scripts')
    <script>
        Swal.fire({
            title: "Berhasil!",
            text: "Data berhasil diperbaharui!",
            icon: "success",
            confirmButtonText: "Lanjutkan",
        });
    </script>
@endpush

@section('content')
    <form action="/kategori/{{$kategori->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')

        <div class="mb-3">
            <label>Nama Kategori</label>
            <input type="text" name="nama" value="{{$kategori->nama}}" placeholder="Enter your new category" class="form-control">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="col-12">
            <button type="submit" class="btn btn-primary">Perbaharui Kategori</button>
            <a href="/kategori" class="btn btn-danger">Batal</a>
        </div>
    </form>
@endsection