@extends('/partials/master')

@section('title')
    Data Kategori Baru
@endsection

@section('judul')
    Kategori Baru
@endsection

@section('content')
    <form action="/kategori" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="mb-3">
            <label>Nama Kategori</label>
            <input type="text" name="nama" placeholder="Enter your new category" class="form-control">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="col-12">
            <button type="submit" class="btn btn-primary">Tambah Kategori</button>
            <a href="/kategori" class="btn btn-danger">Batal</a>
        </div>
    </form>
@endsection