
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Register Forum SanberCode</title>

    <link rel="shortcut icon" href="{{asset('assets/icon.png')}}">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('/adminlte/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('/adminlte/dist/css/adminlte.min.css')}}">

    <style>
        body{
            background-image: url(assets/background.jpg);
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            background-attachment: fixed;
            height: 100%;
        }
    </style>
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <b>Forum Pendaftaran SanberCode</b>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Silahkan Buat Akun Baru Anda!</p>

      <form action="{{ route('register') }}" method="POST">
        @csrf
        <div class="input-group mb-3">
          <input type="text" name="name" class="form-control" placeholder="Username">
          <div class="input-group-append">
            <div class="input-group-text">
                <i class="fas fa-user-tag"></i>
            </div>
          </div>
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="input-group mb-3">
          <input type="email" name="email" class="form-control" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
                <i class="fas fa-envelope"></i>
            </div>
          </div>
        </div>
        @error('email')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
                <i class="fas fa-lock"></i>
            </div>
          </div>
        </div>
        @error('password')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="input-group mb-3">
          <input type="password" name="password_confirmation" class="form-control" placeholder="Konfirmasi Password">
          <div class="input-group-append">
            <div class="input-group-text">
                <i class="fas fa-lock"></i>
            </div>
          </div>
        </div>
        @error('password_confirmation')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="input-group mb-3">
          <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap">
          <div class="input-group-append">
            <div class="input-group-text">
                <i class="fas fa-user-plus"></i>
            </div>
          </div>
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="input-group mb-3">
          <textarea name="bio" class="form-control" id="" placeholder="Biodata"></textarea>
          <div class="input-group-append">
            <div class="input-group-text">
                <i class="fas fa-info-circle"></i>
            </div>
          </div>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="input-group mb-3">
          <input type="text" name="emailProfile" class="form-control" placeholder="Email Profil">
          <div class="input-group-append">
            <div class="input-group-text">
                <i class="fas fa-envelope"></i>
            </div>
          </div>
        </div>
        @error('emailProfile')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="input-group mb-3">
          <input type="number" name="umur" class="form-control" placeholder="Umur">
          <div class="input-group-append">
            <div class="input-group-text">
                <i class="fas fa-user-check"></i>
            </div>
          </div>
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="input-group mb-3">
          <textarea name="alamat" class="form-control" id="" placeholder="Alamat"></textarea>
          <div class="input-group-append">
            <div class="input-group-text">
                <i class="fas fa-map-marked-alt"></i>
            </div>
          </div>
        </div>
        @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="row">
          <div class="col-12">
            <button type="submit" class="btn btn-success btn-block"><i class="fas fa-id-card-alt"></i> | Buat Akun</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <hr>
      <a href="/login" class="btn btn-primary btn-block"><i class="fas fa-user-circle"></i> | Sudah punya akun? Login Disini</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="{{asset('/adminlte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('/adminlte/dist/js/adminlte.min.js')}}"></script>
</body>
</html>
